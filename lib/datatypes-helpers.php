<?php
/**
 * Molotov datatypes helpers.
 *
 * @package wp-plugin-molotov
 */

if ( ! function_exists( 'mlt_register_custom_post' ) ) {
	/**
	 * Helper method to register a custom post type.
	 *
	 * @param string $name The CPT name.
	 * @param array  $configuration The required configuration object.
	 *    'title_plural' => The plural name (string, required).
	 *    'title_singular' => The singular name (string, required).
	 *    'supports' => The supports configuration array (array, optional, default [title, editor, thumbnail, revisions]).
	 *    'dashicon' => The icon (string, optional, defauts to "dashicons-smiley").
	 *    'rewrite' => The rewrite option array (boolean|array, optional, defauts to true).
	 * @param array  $options Array that is merged in register_post_type.
	 * @throws \Exception If a required parameter is missing.
	 * @return void
	 */
	function mlt_register_custom_post( $name, $configuration, $options = array() ) {
		$title_plural   = dig( $configuration, 'title_plural' );
		$title_singular = dig( $configuration, 'title_singular' );
		$supports       = dig(
			$configuration,
			'supports',
			array(
				'title',
				'editor',
				'thumbnail',
				'revisions',
			)
		);
		$taxonomies     = dig( $configuration, 'supports', array() );
		$dashicon       = dig( $configuration, 'dashicon', 'dashicons-smiley' );
		$rewrite        = dig( $configuration, 'rewrite', true );

		if ( ! $title_plural || ! $title_singular ) {
			throw new \Exception( 'title_plural and title_singular are required' );
		}

		if ( ! is_array( $options ) ) {
			throw new \Exception( 'options must be an array' );
		}

		$u_title_plural   = mb_ucfirst( $title_plural );
		$u_title_singular = mb_ucfirst( $title_singular );

		register_post_type(
			$name,
			array_merge_recursive(
				array(
					'labels'              => array(
						'name'                     => __( $u_title_plural, 'molotov' ),
						'singular_name'            => __( $u_title_singular, 'molotov' ),
						'menu_name'                => __( $u_title_plural, 'molotov' ),
						'name_admin_abar'          => __( $u_title_singular, 'molotov' ),
						'add_new'                  => _x( 'Add New', 'molotov' ),
						'add_new_item'             => __( 'Add New ' . $u_title_singular, 'molotov' ),
						'edit_item'                => __( 'Edit ' . $u_title_singular, 'molotov' ),
						'new_item'                 => __( 'New ' . $u_title_singular, 'molotov' ),
						'view_item'                => __( 'View ' . $u_title_singular, 'molotov' ),
						'view_items'               => __( 'View ' . $u_title_plural, 'molotov' ),
						'search_items'             => __( 'Search ' . $u_title_plural, 'molotov' ),
						'not_found'                => __( 'No ' . $title_plural . ' found.', 'molotov' ),
						'not_found_in_trash'       => __( 'No ' . $title_plural . ' found in trash.', 'molotov' ),
						'parent_item_colon'        => __( 'Parent ' . $u_title_singular . ':', 'molotov' ),
						'all_items'                => __( 'All ' . $u_title_plural, 'molotov' ),
						'archives'                 => __( $u_title_plural . ' Archives', 'molotov' ),
						'attributes'               => __( $u_title_singular . ' Attributes', 'molotov' ),
						'insert_into_item'         => __( 'Insert into ' . $title_singular, 'molotov' ),
						'uploaded_to_this_item'    => __( 'Uploaded to this ' . $title_singular, 'molotov' ),
						'item_published'           => __( $u_title_singular . ' published.', 'molotov' ),
						'item_published_privately' => __( $u_title_singular . ' published privately.', 'molotov' ),
						'item_reverted_to_draft'   => __( $u_title_singular . ' reverted to draft.', 'molotov' ),
						'item_scheduled'           => __( $u_title_singular . ' scheduled.', 'molotov' ),
						'item_updated'             => __( $u_title_singular . ' updated.', 'molotov' ),
					),
					'capability_type'     => 'post',
					'description'         => __( 'A ' . $title_singular ),
					'exclude_from_search' => false,
					'has_archive'         => false,
					'hierarchical'        => false,
					'map_meta_cap'        => true,
					'menu_icon'           => $dashicon,
					'menu_position'       => 26,
					'public'              => true,
					'publicly_queryable'  => true,
					'rewrite'             => $rewrite,
					'show_in_admin_bar'   => true,
					'show_in_menu'        => true,
					'show_in_nav_menus'   => true,
					'show_in_rest'        => true,
					'show_ui'             => true,
					'supports'            => $supports,
					'taxonomies'          => $taxonomies,
				),
				$options
			)
		);
	}
}

if ( ! function_exists( 'mlt_register_taxonomy' ) ) {
	/**
	 * Helper method to register a taxonomy.
	 *
	 * @param string $taxonomy The taxonomy name.
	 * @param string $cpt The post type name.
	 * @param array  $configuration The required configuration object.
	 *    'title_plural' => The plural name (string, required).
	 *    'title_singular' => The singular name (string, required).
	 * @param array  $options Array that is merged in register_taxonomy.
	 * @throws \Exception If a required parameter is missing.
	 * @return void
	 */
	function mlt_register_taxonomy( $taxonomy, $cpt, $configuration, $options = array() ) {
		$title_plural   = dig( $configuration, 'title_plural' );
		$title_singular = dig( $configuration, 'title_singular' );

		if ( ! $title_plural || ! $title_singular ) {
			throw new \Exception( 'title_plural and title_singular are required' );
		}

		if ( ! is_array( $options ) ) {
			throw new \Exception( 'options must be an array' );
		}

		$u_title_plural   = ucfirst( $title_plural );
		$u_title_singular = ucfirst( $title_singular );

		register_taxonomy(
			$taxonomy,
			$cpt,
			array_merge_recursive(
				array(
					'labels'             => array(
						'name'                       => _x( $u_title_plural, 'taxonomy general name', 'molotov' ),
						'singular_name'              => _x( $u_title_singular, 'taxonomy singular name', 'molotov' ),
						'all_items'                  => __( 'Tous les ' . $u_title_plural, 'molotov' ),
						'edit_item'                  => __( 'Modifier le ' . $u_title_singular, 'molotov' ),
						'view_item'                  => __( 'Voir le ' . $u_title_singular, 'molotov' ),
						'update_item'                => __( 'Mettre à jour le ' . $u_title_singular, 'molotov' ),
						'add_new_item'               => __( 'Ajouter un ' . $u_title_singular, 'molotov' ),
						'new_item_name'              => __( 'Nouveau nom de ' . $u_title_singular, 'molotov' ),
						'parent_item'                => __( $u_title_singular . ' parent', 'molotov' ),
						'parent_item_colon'          => __( $u_title_singular . ' parent :', 'molotov' ),
						'search_items'               => __( 'Rechercher un ' . $u_title_singular, 'molotov' ),
						'popular_items'              => __( $u_title_plural . ' populaires', 'molotov' ),
						'separate_items_with_commas' => __( 'Séparer les ' . $u_title_plural . ' par une virgule', 'molotov' ),
						'add_or_remove_items'        => __( 'Ajouter ou retirer un ' . $u_title_singular, 'molotov' ),
						'choose_from_most_used'      => __( 'Choisir parmi les ' . $u_title_plural . ' les plus utilisés', 'molotov' ),
						'not_found'                  => __( 'Pas de ' . $u_title_singular . ' trouvé.', 'molotov' ),
						'back_to_items'              => __( '← Retour aux ' . $u_title_plural, 'molotov' ),
					),
					'hierarchical'       => false,
					'meta_box_cb'        => 'post_categories_meta_box',
					'public'             => true,
					'public_queryable'   => true,
					'show_admin_column'  => true,
					'show_in_admin'      => true,
					'show_in_menu'       => true,
					'show_in_nav_menus'  => true,
					'show_in_quick_edit' => true,
					'show_in_rest'       => true,
					'show_ui'            => true,
				),
				$options
			)
		);
	}
}
