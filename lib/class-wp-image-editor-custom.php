<?php
/**
 * Extend WordPress Image Editor class with Molotov behaviour.
 *
 * @package wp-plugin-molotov
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Include the existing classes first in order to extend them.
require_once ABSPATH . WPINC . '/class-wp-image-editor.php';

/**
 * Most changes relate to saving image sizes in their respective folders
 * and generating larger images (upscaling) all media.
 *
 * @package wp-plugin-molotov
 */
class WP_Image_Editor_Custom extends WP_Image_Editor {
	/**
	 * GD Resource.
	 *
	 * @var resource
	 */
	protected $image;


	/**
	 * The current size slug.
	 *
	 * @var string
	 */
	public $size_slug;

	/**
	 * Destructor
	 */
	public function __destruct() {
		if ( $this->image ) {
			// We don't need the original in memory anymore.
			imagedestroy( $this->image );
		}
	}

	/**
	 * Checks to see if current environment supports GD.
	 *
	 * @since 3.5.0
	 *
	 * @param array $args Arguments.
	 * @return bool
	 */
	public static function test( $args = array() ) {
		if ( ! extension_loaded( 'gd' ) || ! function_exists( 'gd_info' ) ) {
			return false;
		}

		// On some setups GD library does not provide imagerotate() - Ticket #11536.
		if ( isset( $args['methods'] ) &&
			in_array( 'rotate', $args['methods'], true ) &&
			! function_exists( 'imagerotate' ) ) {

				return false;
		}

		return true;
	}

	/**
	 * Checks to see if editor supports the mime-type specified.
	 *
	 * @since 3.5.0
	 *
	 * @param string $mime_type Mime type.
	 * @return bool
	 */
	public static function supports_mime_type( $mime_type ) {
		$image_types = imagetypes();
		switch ( $mime_type ) {
			case 'image/jpeg':
				return ( $image_types & IMG_JPG ) !== 0;
			case 'image/png':
				return ( $image_types & IMG_PNG ) !== 0;
			case 'image/gif':
				return ( $image_types & IMG_GIF ) !== 0;
		}

		return false;
	}

	/**
	 * Loads image from $this->file into new GD Resource.
	 *
	 * @since 3.5.0
	 *
	 * @return bool|WP_Error True if loaded successfully; WP_Error on failure.
	 */
	public function load() {
		if ( $this->image ) {
			return true;
		}

		if ( ! is_file( $this->file ) && ! preg_match( '|^https?://|', $this->file ) ) {
			return new WP_Error( 'error_loading_image', __( 'File doesn&#8217;t exist?' ), $this->file );
		}

		// Set artificially high because GD uses uncompressed images in memory.
		wp_raise_memory_limit( 'image' );

		$this->image = @imagecreatefromstring( file_get_contents( $this->file ) );

		if ( ! is_resource( $this->image ) ) {
			return new WP_Error( 'invalid_image', __( 'File is not an image.' ), $this->file );
		}

		$size = @getimagesize( $this->file );
		if ( ! $size ) {
			return new WP_Error( 'invalid_image', __( 'Could not read image size.' ), $this->file );
		}

		if ( function_exists( 'imagealphablending' ) && function_exists( 'imagesavealpha' ) ) {
			imagealphablending( $this->image, false );
			imagesavealpha( $this->image, true );
		}

		$this->update_size( $size[0], $size[1] );
		$this->mime_type = $size['mime'];

		return $this->set_quality();
	}

	/**
	 * Sets or updates current image size.
	 *
	 * @since 3.5.0
	 *
	 * @param int $width Width.
	 * @param int $height Height.
	 * @return true
	 */
	protected function update_size( $width = false, $height = false ) {
		if ( ! $width ) {
			$width = imagesx( $this->image );
		}

		if ( ! $height ) {
			$height = imagesy( $this->image );
		}

		return parent::update_size( $width, $height );
	}

	/**
	 * Resizes current image even to a bigger format.
	 * Wraps _resize, since _resize returns a GD Resource.
	 *
	 * At minimum, either a height or width must be provided.
	 * If one of the two is set to null, the resize will
	 * maintain aspect ratio according to the provided dimension.
	 *
	 * @param  int|null $max_w Image width.
	 * @param  int|null $max_h Image height.
	 * @param  bool     $crop  Should crop or not.
	 * @return true|WP_Error The result.
	 */
	public function resize( $max_w, $max_h, $crop = false ) {
		$image_sizes    = $this->get_all_image_sizes();
		$max_required_w = array_reduce(
			$image_sizes,
			function ( $max, $size ) {
				if ( intval( $size['width'] ) > $max ) {
					return intval( $size['width'] );
				}

				return $max;
			},
			0
		);
		$max_required_h = array_reduce(
			$image_sizes,
			function ( $max, $size ) {
				if ( intval( $size['height'] ) > $max ) {
					return intval( $size['height'] );
				}

				return $max;
			},
			0
		);

		$no_cap_max_w = ( $max_w >= $max_required_w ) ? $max_w : 10 * $max_w;
		$no_cap_max_h = ( $max_h >= $max_required_h ) ? $max_h : 10 * $max_h;

		$resized = $this->_resize( $no_cap_max_w, $no_cap_max_h, $crop );

		if ( is_resource( $resized ) ) {
			imagedestroy( $this->image );
			$this->image = $resized;
			return true;

		} elseif ( is_wp_error( $resized ) ) {
			return $resized;
		}

		return new WP_Error( 'image_resize_error', __( 'Image resize failed.' ), $this->file );
	}

	/**
	 * Private resize method. It calls our custom_image_resize_dimensions method.
	 *
	 * @param int        $max_w  Maximum width.
	 * @param int        $max_h  Maximum height.
	 * @param bool|array $crop   Should we crop.
	 * @return resource|WP_Error The result resource or an error.
	 */
	protected function _resize( $max_w, $max_h, $crop = false ) {
		$dims = $this->custom_image_resize_dimensions( $this->size['width'], $this->size['height'], $max_w, $max_h, $crop );

		if ( ! $dims ) {
			return new WP_Error( 'error_getting_dimensions', __( 'Could not calculate resized image dimensions' ), $this->file );
		}
		list( $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h ) = $dims;

		$resized = wp_imagecreatetruecolor( $dst_w, $dst_h );
		imagecopyresampled( $resized, $this->image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h );

		if ( is_resource( $resized ) ) {
			$this->update_size( $dst_w, $dst_h );
			return $resized;
		}

		return new WP_Error( 'image_resize_error', __( 'Image resize failed.' ), $this->file );
	}

	/**
	 * Create an image with specified size and return the image meta data value for it.
	 *
	 * @param string $size The size name.
	 * @param array  $size_data Array of width, height, and whether to crop.
	 * @return WP_Error|array WP_Error on error, or the image data array for inclusion in the `sizes` array in the image meta.
	 */
	public function make_image_size( $size, $size_data ) {
		$this->size_slug = $size;

		if ( ! isset( $size_data['width'] ) && ! isset( $size_data['height'] ) ) {
			return new WP_Error( 'image_subsize_create_error', __( 'Cannot resize the image. Both width and height are not set.' ) );
		}

		$orig_size = $this->size;

		if ( ! isset( $size_data['width'] ) ) {
			$size_data['width'] = null;
		}

		if ( ! isset( $size_data['height'] ) ) {
			$size_data['height'] = null;
		}

		if ( ! isset( $size_data['crop'] ) ) {
			$size_data['crop'] = false;
		}

		$resized = $this->_resize( $size_data['width'], $size_data['height'], $size_data['crop'] );

		if ( is_wp_error( $resized ) ) {
			$saved = $resized;
		} else {
			$saved = $this->_save( $resized );
			imagedestroy( $resized );
		}

		$this->size = $orig_size;

		if ( ! is_wp_error( $saved ) ) {
			unset( $saved['path'] );
		}

		return $saved;
	}

	/**
	 * Crops Image.
	 *
	 * @since 3.5.0
	 *
	 * @param int  $src_x   The start x position to crop from.
	 * @param int  $src_y   The start y position to crop from.
	 * @param int  $src_w   The width to crop.
	 * @param int  $src_h   The height to crop.
	 * @param int  $dst_w   Optional. The destination width.
	 * @param int  $dst_h   Optional. The destination height.
	 * @param bool $src_abs Optional. If the source crop points are absolute.
	 * @return bool|WP_Error
	 */
	public function crop( $src_x, $src_y, $src_w, $src_h, $dst_w = null, $dst_h = null, $src_abs = false ) {
		// If destination width/height isn't specified, use same as
		// width/height from source.
		if ( ! $dst_w ) {
			$dst_w = $src_w;
		}
		if ( ! $dst_h ) {
			$dst_h = $src_h;
		}

		$dst = wp_imagecreatetruecolor( $dst_w, $dst_h );

		if ( $src_abs ) {
			$src_w -= $src_x;
			$src_h -= $src_y;
		}

		if ( function_exists( 'imageantialias' ) ) {
			imageantialias( $dst, true );
		}

		imagecopyresampled( $dst, $this->image, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h );

		if ( is_resource( $dst ) ) {
			imagedestroy( $this->image );
			$this->image = $dst;
			$this->update_size();
			return true;
		}

		return new WP_Error( 'image_crop_error', __( 'Image crop failed.' ), $this->file );
	}

	/**
	 * Rotates current image counter-clockwise by $angle.
	 * Ported from image-edit.php
	 *
	 * @since 3.5.0
	 *
	 * @param float $angle Angle of rotation.
	 * @return true|WP_Error
	 */
	public function rotate( $angle ) {
		if ( function_exists( 'imagerotate' ) ) {
			$transparency = imagecolorallocatealpha( $this->image, 255, 255, 255, 127 );
			$rotated      = imagerotate( $this->image, $angle, $transparency );

			if ( is_resource( $rotated ) ) {
				imagealphablending( $rotated, true );
				imagesavealpha( $rotated, true );
				imagedestroy( $this->image );
				$this->image = $rotated;
				$this->update_size();
				return true;
			}
		}
		return new WP_Error( 'image_rotate_error', __( 'Image rotate failed.' ), $this->file );
	}

	/**
	 * Flips current image.
	 *
	 * @since 3.5.0
	 *
	 * @param bool $horz Flip along Horizontal Axis.
	 * @param bool $vert Flip along Vertical Axis.
	 * @return true|WP_Error
	 */
	public function flip( $horz, $vert ) {
		$w   = $this->size['width'];
		$h   = $this->size['height'];
		$dst = wp_imagecreatetruecolor( $w, $h );

		if ( is_resource( $dst ) ) {
			$sx = $vert ? ( $w - 1 ) : 0;
			$sy = $horz ? ( $h - 1 ) : 0;
			$sw = $vert ? -$w : $w;
			$sh = $horz ? -$h : $h;

			if ( imagecopyresampled( $dst, $this->image, 0, 0, $sx, $sy, $w, $h, $sw, $sh ) ) {
				imagedestroy( $this->image );
				$this->image = $dst;
				return true;
			}
		}
		return new WP_Error( 'image_flip_error', __( 'Image flip failed.' ), $this->file );
	}

	/**
	 * Saves current in-memory image to file.
	 *
	 * @since 3.5.0
	 *
	 * @param string|null $filename Filename.
	 * @param string|null $mime_type Mime type.
	 * @return array|WP_Error {'path'=>string, 'file'=>string, 'width'=>int, 'height'=>int, 'mime-type'=>string}
	 */
	public function save( $filename = null, $mime_type = null ) {
		$saved = $this->_save( $this->image, $filename, $mime_type );

		if ( ! is_wp_error( $saved ) ) {
			$this->file      = $saved['path'];
			$this->mime_type = $saved['mime-type'];
		}

		return $saved;
	}

	/**
	 * Actually save the file.
	 *
	 * @param resource    $image     An image resource.
	 * @param string|null $filename  The filename.
	 * @param string|null $mime_type Mime type.
	 * @return WP_Error|array
	 */
	protected function _save( $image, $filename = null, $mime_type = null ) {
		list( $filename, $extension, $mime_type ) = $this->get_output_format( $filename, $mime_type );

		if ( ! $filename ) {
			$filename = $this->generate_filename( null, null, $extension );
		}

		if ( 'image/gif' === $mime_type ) {
			if ( ! $this->make_image( $filename, 'imagegif', array( $image, $filename ) ) ) {
				return new WP_Error( 'image_save_error', __( 'Image Editor Save Failed' ) );
			}
		} elseif ( 'image/png' === $mime_type ) {
			// convert from full colors to index colors, like original PNG.
			if ( function_exists( 'imageistruecolor' ) && ! imageistruecolor( $image ) ) {
				imagetruecolortopalette( $image, false, imagecolorstotal( $image ) );
			}

			if ( ! $this->make_image( $filename, 'imagepng', array( $image, $filename ) ) ) {
				return new WP_Error( 'image_save_error', __( 'Image Editor Save Failed' ) );
			}
		} elseif ( 'image/jpeg' === $mime_type ) {
			if ( ! $this->make_image( $filename, 'imagejpeg', array( $image, $filename, $this->get_quality() ) ) ) {
				return new WP_Error( 'image_save_error', __( 'Image Editor Save Failed' ) );
			}
		} else {
			return new WP_Error( 'image_save_error', __( 'Image Editor Save Failed' ) );
		}

		// Set correct file permissions.
		$stat  = stat( dirname( $filename ) );
		$perms = $stat['mode'] & 0000666; // Same permissions as parent folder, strip off the executable bits.
		chmod( $filename, $perms );

		/**
		 * Filters the name of the saved image file.
		 *
		 * @since 2.6.0
		 *
		 * @param string $filename Name of the file.
		 */
		return array(
			'path'      => $filename,
			'file'      => wp_basename( apply_filters( 'image_make_intermediate_size', $filename ) ),
			'width'     => $this->size['width'],
			'height'    => $this->size['height'],
			'mime-type' => $mime_type,
		);
	}

	/**
	 * Returns stream of current image.
	 *
	 * @since 3.5.0
	 *
	 * @param string $mime_type The mime type of the image.
	 * @return bool True on success, false on failure.
	 */
	public function stream( $mime_type = null ) {
		list( $filename, $extension, $mime_type ) = $this->get_output_format( null, $mime_type );

		switch ( $mime_type ) {
			case 'image/png':
				header( 'Content-Type: image/png' );
				return imagepng( $this->image );
			case 'image/gif':
				header( 'Content-Type: image/gif' );
				return imagegif( $this->image );
			default:
				header( 'Content-Type: image/jpeg' );
				return imagejpeg( $this->image, null, $this->get_quality() );
		}
	}

	/**
	 * Either calls editor's save function or handles file as a stream.
	 *
	 * @since 3.5.0
	 *
	 * @param string|stream $filename  Filename.
	 * @param callable      $function  A callaback method.
	 * @param array         $arguments Arguments for callback.
	 * @return bool
	 */
	protected function make_image( $filename, $function, $arguments ) {
		if ( wp_is_stream( $filename ) ) {
			$arguments[1] = null;
		}

		return parent::make_image( $filename, $function, $arguments );
	}

	/**
	 * Builds an output filename based on current file
	 * The file path includes the format.
	 *
	 * @since 3.5.0
	 *
	 * @param string $suffix Append to filename.
	 * @param string $dest_path Destinatino path.
	 * @param string $extension File extension.
	 * @return string filename Full path.
	 */
	public function generate_filename( $suffix = null, $dest_path = null, $extension = null ) {
		if ( ! $suffix ) {
			$suffix = $this->get_suffix();
		}

		$dir = pathinfo( $this->file, PATHINFO_DIRNAME );
		$ext = pathinfo( $this->file, PATHINFO_EXTENSION );

		$name    = wp_basename( $this->file, ".$ext" );
		$new_ext = strtolower( $extension ? $extension : $ext );

		if ( ! is_null( $dest_path ) ) {
			$_dest_path = realpath( $dest_path );
			if ( $_dest_path ) {
				$dir = $_dest_path;
			}
		}

		$size_from_suffix = explode( 'x', $suffix );

		return trailingslashit( $dir ) . "{$this->size_slug}/{$name}.{$new_ext}";
	}

	/**
	 * Create multiple images from a single source.
	 *
	 * Attempts to create all sub-sizes and returns the meta data at the end. This
	 * may result in the server running out of resources. When it fails there may be few
	 * "orphaned" images left over as the meta data is never returned and saved.
	 *
	 * Add the slug to the filepath.
	 *
	 * @param array $sizes {
	 *     An array of image size data arrays.
	 *
	 *     Either a height or width must be provided.
	 *     If one of the two is set to null, the resize will
	 *     maintain aspect ratio according to the source image.
	 *
	 *     @type array $size {
	 *         Array of height, width values, and whether to crop.
	 *
	 *         @type int  $width  Image width. Optional if `$height` is specified.
	 *         @type int  $height Image height. Optional if `$width` is specified.
	 *         @type bool $crop   Optional. Whether to crop the image. Default false.
	 *     }
	 * }
	 * @return array An array of resized images' metadata by size.
	 */
	public function multi_resize( $sizes ) {
		$metadata = array();

		foreach ( $sizes as $size => $size_data ) {
			$meta = $this->make_image_size( $size, $size_data );

			if ( ! is_wp_error( $meta ) ) {
				$metadata[ $size ] = $meta;
			}
		}

		return $metadata;
	}

	/**
	 * Retrieves calculated resize dimensions for use in WP_Image_Editor_Custom.
	 *
	 * Main difference is that we don't return if the source image is smaller than
	 * the target image thus allowing upscaling.
	 *
	 * @param int        $orig_w Original width in pixels.
	 * @param int        $orig_h Original height in pixels.
	 * @param int        $dest_w New width in pixels.
	 * @param int        $dest_h New height in pixels.
	 * @param bool|array $crop   Optional. Whether to crop image to specified width and height or resize.
	 *                           An array can specify positioning of the crop area. Default false.
	 * @return false|array False on failure. Returned array matches parameters for `imagecopyresampled()`.
	 */
	private function custom_image_resize_dimensions( $orig_w, $orig_h, $dest_w, $dest_h, $crop = false ) {
		/**
		 * Filters whether to preempt calculating the image resize dimensions.
		 *
		 * Passing a non-null value to the filter will effectively short-circuit
		 * image_resize_dimensions(), returning that value instead.
		 *
		 * @since 3.4.0
		 *
		 * @param null|mixed $null   Whether to preempt output of the resize dimensions.
		 * @param int        $orig_w Original width in pixels.
		 * @param int        $orig_h Original height in pixels.
		 * @param int        $dest_w New width in pixels.
		 * @param int        $dest_h New height in pixels.
		 * @param bool|array $crop   Whether to crop image to specified width and height or resize.
		 *                           An array can specify positioning of the crop area. Default false.
		 */

		if ( $crop ) {
			// Crop the largest possible portion of the original image that we can size to $dest_w x $dest_h.
			$aspect_ratio = $orig_w / $orig_h;
			if ( $orig_w < $dest_w ) {
				$new_w = $dest_w;
			} else {
				$new_w = min( $dest_w, $orig_w );
			}
			if ( $orig_h < $dest_h ) {
				$new_h = $dest_h;
			} else {
				$new_h = min( $dest_h, $orig_h );
			}

			if ( ! $new_w ) {
				$new_w = (int) round( $new_h * $aspect_ratio );
			}

			if ( ! $new_h ) {
				$new_h = (int) round( $new_w / $aspect_ratio );
			}

			$size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );

			$crop_w = round( $new_w / $size_ratio );
			$crop_h = round( $new_h / $size_ratio );

			if ( ! is_array( $crop ) || count( $crop ) !== 2 ) {
				$crop = array( 'center', 'center' );
			}

			list( $x, $y ) = $crop;

			if ( 'left' === $x ) {
				$s_x = 0;
			} elseif ( 'right' === $x ) {
				$s_x = $orig_w - $crop_w;
			} else {
				$s_x = floor( ( $orig_w - $crop_w ) / 2 );
			}

			if ( 'top' === $y ) {
				$s_y = 0;
			} elseif ( 'bottom' === $y ) {
				$s_y = $orig_h - $crop_h;
			} else {
				$s_y = floor( ( $orig_h - $crop_h ) / 2 );
			}
		} else {
			// Don't crop, just resize using $dest_w x $dest_h as a maximum bounding box.
			$crop_w = $orig_w;
			$crop_h = $orig_h;

			$s_x = 0;
			$s_y = 0;

			list( $new_w, $new_h ) = $this->custom_wp_constrain_dimensions( $orig_w, $orig_h, $dest_w, $dest_h );
		}

		// The return array matches the parameters to imagecopyresampled().
		// int dst_x, int dst_y, int src_x, int src_y, int dst_w, int dst_h, int src_w, int src_h.
		return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}

	/**
	 * Calculates the new dimensions for a down- or up-sampled image.
	 *
	 * @param int $current_width  Current width of the image.
	 * @param int $current_height Current height of the image.
	 * @param int $max_width      Optional. Max width in pixels to constrain to. Default 0.
	 * @param int $max_height     Optional. Max height in pixels to constrain to. Default 0.
	 * @return array First item is the width, the second item is the height.
	 */
	private function custom_wp_constrain_dimensions( $current_width, $current_height, $max_width = 0, $max_height = 0 ) {
		if ( $max_width && ! $max_height ) {
			return array( $max_width, floor( $current_height * $max_width / $current_width ) );
		}

		if ( ! $max_width && ! $max_height ) {
			return array( $current_width, $current_height );
		}
		$height_ratio = 1.0;
		$width_ratio  = $height_ratio;
		$did_height   = false;
		$did_width    = $did_height;

		if ( $max_width > 0 && $current_width > 0 ) {
			$width_ratio = $max_width / $current_width;
			$did_width   = true;
		}

		if ( $max_height > 0 && $current_height > 0 ) {
			$height_ratio = $max_height / $current_height;
			$did_height   = true;
		}

		// Calculate the larger/smaller ratios.
		$smaller_ratio = min( $width_ratio, $height_ratio );
		$larger_ratio  = max( $width_ratio, $height_ratio );

		if ( (int) round( $current_width * $larger_ratio ) > $max_width || (int) round( $current_height * $larger_ratio ) > $max_height ) {
			// The larger ratio is too big. It would result in an overflow.
			$ratio = $smaller_ratio;
		} else {
			// The larger ratio fits, and is likely to be a more "snug" fit.
			$ratio = $larger_ratio;
		}

		// Very small dimensions may result in 0, 1 should be the minimum.
		$w = max( 1, (int) round( $current_width * $ratio ) );
		$h = max( 1, (int) round( $current_height * $ratio ) );

		// Sometimes, due to rounding, we'll end up with a result like this: 465x700 in a 177x177 box is 117x176... a pixel short.
		// We also have issues with recursive calls resulting in an ever-changing result. Constraining to the result of a constraint should yield the original result.
		// Thus we look for dimensions that are one pixel shy of the max value and bump them up.

		// Note: $did_width means it is possible $smaller_ratio == $width_ratio.
		if ( $did_width && $w === $max_width - 1 ) {
			$w = $max_width;
		}

		// Note: $did_height means it is possible $smaller_ratio == $height_ratio.
		if ( $did_height && $h === $max_height - 1 ) {
			$h = $max_height;
		}

		/**
		 * Filters dimensions to constrain down-sampled images to.
		 *
		 * @since 4.1.0
		 *
		 * @param array $dimensions     The image width and height.
		 * @param int   $current_width  The current width of the image.
		 * @param int   $current_height The current height of the image.
		 * @param int   $max_width      The maximum width permitted.
		 * @param int   $max_height     The maximum height permitted.
		 */
		return apply_filters( 'wp_constrain_dimensions', array( $w, $h ), $current_width, $current_height, $max_width, $max_height );
	}

	/**
	 * Return all image sizes available.
	 *
	 * @return array The image sizes.
	 */
	private function get_all_image_sizes() {
		$_wp_additional_image_sizes = wp_get_additional_image_sizes();

		$image_sizes = array();
		foreach ( get_intermediate_image_sizes() as $s ) {
			$image_sizes[ $s ] = array(
				'width'  => '',
				'height' => '',
				'crop'   => false,
			);
			if ( isset( $_wp_additional_image_sizes[ $s ]['width'] ) ) {
				$image_sizes[ $s ]['width'] = intval( $_wp_additional_image_sizes[ $s ]['width'] );
			} else {
				$image_sizes[ $s ]['width'] = get_option( "{$s}_size_w" );
			}

			if ( isset( $_wp_additional_image_sizes[ $s ]['height'] ) ) {
				$image_sizes[ $s ]['height'] = intval( $_wp_additional_image_sizes[ $s ]['height'] );
			} else {
				$image_sizes[ $s ]['height'] = get_option( "{$s}_size_h" );
			}

			if ( isset( $_wp_additional_image_sizes[ $s ]['crop'] ) ) {
				$image_sizes[ $s ]['crop'] = $_wp_additional_image_sizes[ $s ]['crop'];
			} else {
				$image_sizes[ $s ]['crop'] = get_option( "{$s}_crop" );
			}

			$image_sizes[ $s ]['width']  = intval( $image_sizes[ $s ]['width'] );
			$image_sizes[ $s ]['height'] = intval( $image_sizes[ $s ]['height'] );
		}

		return $image_sizes;
	}
}
