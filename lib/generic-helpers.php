<?php
/**
 * Molotov generic helpers.
 *
 * @package wp-plugin-molotov
 */

if ( ! function_exists( 'mb_ucfirst' ) ) {
	/**
	 * The same as ucfirst but for multibyte strings.
	 *
	 * @param string $string Input string.
	 * @param string $encoding Optional encoding. Defaults to uft-8, almost always the right one.
	 * @return string The string with the first letter uppercased.
	 */
	function mb_ucfirst( $string, $encoding = 'utf-8' ) {
		$strlen     = mb_strlen( $string, $encoding );
		$first_char = mb_substr( $string, 0, 1, $encoding );
		$then       = mb_substr( $string, 1, $strlen - 1, $encoding );
		return mb_strtoupper( $first_char, $encoding ) . $then;
	}
}

if ( ! function_exists( 'dig' ) ) {
	/**
	 * Dig into an array to find a parameter, recursively.
	 * Avoids nested issets and such constructs.
	 *
	 * @param array    $target The array to dig in.
	 * @param string   $key A dot separated string of keys.
	 * @param null|any $default A default return value.
	 * @return null|any The value at the key or $default.
	 */
	function dig( $target, $key, $default = null ) {
		$parts = explode( '.', $key, 2 );

		if ( count( $parts ) === 1 ) {
			if ( isset( $target[ $key ] ) && null !== $target[ $key ] ) {
				return $target[ $key ];
			}
		}

		$key  = $parts[0];
		$rest = isset( $parts[1] ) ? $parts[1] : '';

		if ( isset( $target[ $key ] ) && null !== $target[ $key ] ) {
			return dig( $target[ $key ], $rest, $default );
		}

		return $default;
	}
}

if ( ! function_exists( 'p' ) ) {
	/**
	 * Debugging shortcut for inserting a pre block.
	 *
	 * @param string $print The content to print.
	 * @return void
	 */
	function p( $print ) {
		echo '<pre>';
		print_r( $print );
		echo '</pre>';
	}
}
