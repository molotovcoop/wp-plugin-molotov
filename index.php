<?php
/**
 * Avoid directory listing on misconfigured webservers.
 *
 * @package wp-plugin-molotov
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
