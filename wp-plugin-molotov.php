<?php
/**
 * Plugin Name: Molotov WordPress Plugin
 * Description: Plugin WordPress de la coopérative Molotov.
 * Author: Molotov
 * Author URI: https://molotov.ca
 * Version: 0.1
 *
 * @package wp-plugin-molotov
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once( 'lib/class-wp-image-editor-custom.php' );
require_once( 'lib/generic-helpers.php' );
require_once( 'lib/datatypes-helpers.php' );

add_filter( 'wp_image_editors', 'molotov_wp_image_editors' );
add_filter(
	'wp_generate_attachment_metadata',
	function ( $metadata, $attachment_id, $context = null ) {
		foreach ( dig( $metadata, 'sizes', array() ) as $slug => &$size ) {
			$size['file'] = "$slug/{$size['file']}";
		}
		return $metadata;
	},
	20,
	4
);

/**
 * Register our custom Image Editor class
 *
 * @param array $editors Image editor classes.
 * @return array
 */
function molotov_wp_image_editors( $editors ) {
	array_unshift( $editors, 'WP_Image_Editor_Custom' );
	return $editors;
}
